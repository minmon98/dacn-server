const express = require("express")
const bodyParser = require("body-parser")

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

const controllers = require(__dirname + "/apps/controllers")
app.use(controllers)

app.listen(3000, () => {
    console.log("Server is running")
})