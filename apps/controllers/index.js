const express = require("express")
const downloadService = require("../services/download_service")
const tesseract = require("node-tesseract-ocr")

var router = express.Router()

const config = {
    lang: "vie",
    oem: 1,
    psm: 3,
}

router.post("/uploads", async (req,res) => {
    // Download image
    let url = req.body.image_url
    let image_path = "public/downloads/image.png"
    downloadService.download_image(url, image_path)
    
    // OCR image
    let result = await tesseract.recognize(image_path, config)
    let sentences = result.split("\n")
    // res.json({ 
    //     success: true,
    //     sentences 
    // })
    let name = "Trống"
    let email = "Trống"
    let phone = "Trống"
    let company = "Trống"

    var emailRe = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/
    var emailArr = result.match(emailRe)
    
    var phoneRe = /(\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4}))|(\+?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4}))|(\+?([0-9]{4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3}))/
    var phoneArr = result.match(phoneRe)

    var companyRe = /(ngân hàng)|(bệnh viên)|(khách sạn)|(nhà hàng)|(công ty)|(tập đoàn)|(doanh nghiệp)|(bưu điện)|(group)|(JSC)|(banks)|(hostpital)|(đài truyền hình)|(bar)/
    for (var index = 0; index < sentences.length; index++) {
        let companyArr = sentences[index].toLowerCase().match(companyRe)
        if (companyArr != null && companyArr.length > 0) {
            company = sentences[index].toUpperCase()
                .replace(/(\!|\"|\#|\$|\%|\&|\'|\(|\)|\*|\+|\,|\-|\.|\/|\:|\;|\<|\=|\>|\?|\@|\[|\]|\\|\^|\_|\`|\{|\}|\||\~)/g, "")
        } 
    }    

    if (emailArr != null) {
        if (emailArr.length > 0) {
            email = emailArr[0]
        } else {
            email = "Trống"
        }
    } else {
        email = "Trống"
    }

    if (phoneArr != null) {
        if (phoneArr.length > 0) {
            phone = phoneArr[0]    
        } else {
            phone = "Trống"
        }
    } else {
        phone = "Trống"
    }

    res.json({
        name: "Lý Ngọc Tú",
        email: email,
        phone: phone,
        company: company,
        sentences: sentences
    })
})

module.exports = router